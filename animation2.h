//
//  animation2.h
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface animation2 : NSObject<UIViewControllerAnimatedTransitioning>
typedef NS_ENUM(NSUInteger, KWTransitionStep){
    KWTransitionStepDismiss = 0,	/* Moving back to the inital step */
    KWTransitionStepPresent		/* Moving to the modal */
};
typedef NS_ENUM(NSUInteger, KWTransitionStyle){

    KWTransitionStyleUp
};
typedef NS_OPTIONS(NSUInteger, KWTransitionSetting){
    KWTransitionSettingNone = 0,
    KWTransitionSettingDirectionRight = 1 << 0,
    KWTransitionSettingDirectionLeft = 1 << 1,
    KWTransitionSettingDirectionDown = 1 << 2,
    KWTransitionSettingDirectionUp = 1 << 3,
    KWTransitionSettingReverse = 1 << 5
};
/// Transition the view controller to this step.
@property (nonatomic) KWTransitionStep action;

/// Style the transition this way
@property (nonatomic) KWTransitionStyle style;

/// The duration of the transition
@property (nonatomic) NSTimeInterval duration;

/// The settings of the transitions
@property (nonatomic) KWTransitionSetting settings;


@end
