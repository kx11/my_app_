//
//  animation1.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "animation6.h"

@implementation animation6
- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 2.0;
}
// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [[transitionContext containerView] addSubview:toVC.view];
    if ( self.isdismissal ) {
        [[transitionContext containerView] sendSubviewToBack:toVC.view];
    }
    
    toVC.view.alpha = 0.0f;
    if ( !self.isdismissal ) {
        toVC.view.transform = CGAffineTransformMakeScale(0.8, 0.8);
    } else {
        toVC.view.transform = CGAffineTransformMakeScale(1.2, 1.2);
    }
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0
                        options:0
                     animations:^{
                         
                         toVC.view.alpha = 1.0f;
                         toVC.view.transform = CGAffineTransformIdentity;
                         
                         if ( !self.isdismissal ) {
                             fromVC.view.transform = CGAffineTransformMakeScale(1.2, 1.2);
                         } else {
                             fromVC.view.alpha = 0.0f;
                             fromVC.view.transform = CGAffineTransformMakeScale(0.6, 0.6);
                         }
                     }
                     completion:^(BOOL finished) {
                         self.interacting = NO;
                         fromVC.view.transform = CGAffineTransformIdentity;
                         if ( ![transitionContext transitionWasCancelled]) {
                             [fromVC.view removeFromSuperview];
                         }
                         [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
                     }];
    
}

@end
