//
//  note.swift
//  Lock notes
//
//  Created by 伊藤慶 on 2015/09/13.
//  Copyright (c) 2015年 伊藤慶. All rights reserved.
//

import UIKit
var allNotes:[Note] = []
//var allNotes2:[Dates] = []
var Noteindex:Int = -1
var NoteTable:UITableView?
let kAllNotes:String = "notes"
let kAllDates:String = "dates"
class Note: NSObject {
    var note:String
    override init(){
        note = ""
    }
    func dictionary() -> NSDictionary{
        return ["note":note]
    }
    
    class func saveNotes() {
        var aDictionaries:[NSDictionary] = []
        for i:Int in 0 ..< allNotes.count {
            aDictionaries.append(allNotes[i].dictionary())
            print(allNotes[i].dictionary())
        }
        UserDefaults.standard.set(aDictionaries, forKey: kAllNotes)
    }
    class func loadNotes() {
        allNotes.removeAll()
        let defaults:UserDefaults = UserDefaults.standard
        let saveData:[NSDictionary]? = defaults.object(forKey: kAllNotes) as? [NSDictionary]
        print(saveData)
        if let date:[NSDictionary] = saveData {
            print("aaa",date.count)
            for i:Int in 0 ..< date.count {
                let n:Note = Note()
                n.setValuesForKeys(date[i] as! [String : AnyObject])
                allNotes.append(n)
                
            }
        }
    }
}
/*
class Dates: NSObject {
    var date:String!
    override init(){
        date = ""
    }
    func dictionary() -> NSDictionary{
        return ["data":date]
    }
    
    class func saveNotes() {
        var aDictionaries:[NSDictionary] = []
        for i:Int in 0 ..< allNotes2.count {
            aDictionaries.append(allNotes2[i].dictionary())
            print(allNotes2[i].dictionary())
        }
        UserDefaults.standard.set(aDictionaries, forKey: kAllDates)
    }
    class func loadNotes() {
        allNotes2.removeAll()
        let defaults:UserDefaults = UserDefaults.standard
        let saveData:[NSDictionary]? = defaults.object(forKey: kAllDates) as? [NSDictionary]
        print(saveData)
        if let date:[NSDictionary] = saveData {
            print("aaa",date.count)
            for i:Int in 0 ..< date.count {
                let n:Dates = Dates()
//                n.setValuesForKeys(date[i] as String)
                allNotes2.append(date[i])
            }
        }
    }
}
 */

