//
//  Step3ViewController.m
//  パスワードsample
//
//  Created by 伊藤慶 on 2016/05/21.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "Step3ViewController.h"

@interface Step3ViewController ()

@end

@implementation Step3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults=[NSUserDefaults standardUserDefaults];
    // Do any additional setup after loading the view.
    image[0] = [UIImage imageNamed:@"右.jpg"];
    image[1] = [UIImage imageNamed:@"左.jpg"];
    image[2] = [UIImage imageNamed:@"上.jpg"];
    image[3] = [UIImage imageNamed:@"下.jpg"];
    image[4] = [UIImage imageNamed:@"時計回り.jpg"];
    image[5] = [UIImage imageNamed:@"反時計回り.jpg"];
    image[6] = [UIImage imageNamed:@"外側.jpg"];
    image[7] = [UIImage imageNamed:@"内側.jpg"];
    
    
    // Do any additional setup after loading the view.
    //Pinch（2本の指でつまむ）の認識
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [self.view addGestureRecognizer:pinchRecognizer];
    
    //Rotate（2本の指で回転）の認識
    UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetected:)];
    [self.view addGestureRecognizer:rotationRecognizer];
    
    //右向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightDetected:)];
    swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRightRecognizer];
    
    //左向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftDetected:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    
    //上向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDetected:)];
    swipeUpRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUpRecognizer];
    
    //下向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeDownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownDetected:)];
    swipeDownRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownRecognizer];
    _volume=true;
    _hantei=true;
    _label.hidden=YES;
    count=0;
    _imageview.hidden=YES;
    array= [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)next:(id)sender {
    _volume=false;

        _label.hidden=NO;
        _label.text=@"もう一度";
        _imageview.hidden=YES;
    if (_ok==true) {
        NSData *listData = [NSKeyedArchiver archivedDataWithRootObject:array];
        [defaults setObject:listData forKey:@"KEY"];
        [defaults synchronize];
        [self performSegueWithIdentifier:@"next" sender:self];
    }
    
    
}
-(void)unRock{
    
}
-(void)step:(NSString *)string{
    [array addObject:string];
    NSLog(@"%@",string);
    _imageview.hidden=NO;
    
    NSLog(@"%lu",(unsigned long)array.count);
    _imageview.image = image[[string intValue]];
    
}
-(void)password:(NSString *)string{
    _label.hidden=YES;

    if (_hantei==true) {
//        NSLog(@"aaaa%@",array[0]);
//        NSLog(@"%@",array[1]);
//        NSLog(@"%@",array[2]);
//        NSLog(@"%@",array[3]);
//        NSLog(@"%@",array[4]);
//        NSLog(@"%@",array[5]);
//        NSLog(@"%@",array[6]);
//        NSLog(@"%@",array[7]);
    if (string==array[count]) {
        count++;
        NSLog(@"カウントの数は%d",count);
        if (array.count == count) {
            NSLog(@"ロック解除3");
             _hantei=false;
            _ok=true;

        }
    }else{
        NSLog(@"ちがいます");
        _hantei=false;
    }
    }
    
}

- (IBAction)swipeRightDetected:(UIGestureRecognizer *)sender {
    NSLog(@"右向きSwipe");
    if (_volume==true) {
    [self step:@"0"];
    }else{
    [self password:@"0"];
    }
    
}

//左向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeLeftDetected:(UIGestureRecognizer *)sender {
    NSLog(@"左向きSwipe");
    if (_volume==true) {
        [self step:@"1"];
    }else{
        [self password:@"1"];
    }
}

//上向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeUpDetected:(UIGestureRecognizer *)sender {
    NSLog(@"上向きSwipe");
    if (_volume==true) {
        [self step:@"2"];
    }else{
        [self password:@"2"];
    }
    
}

//下向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeDownDetected:(UIGestureRecognizer *)sender {
    NSLog(@"下向きSwipe");
    if (_volume==true) {
        [self step:@"3"];
    }else{
        [self password:@"3"];
    }
    
    
}

- (IBAction)rotationDetected:(UIGestureRecognizer *)sender {
    //Rotate開始時から見た回転の度合い（ラジアン）
    CGFloat radians = [(UIRotationGestureRecognizer *)sender rotation];
    //「ラジアン」を「度」に変換
    CGFloat degrees = radians * (180/M_PI);
    if (degrees > 90) {
        if (_volume==true) {
            [self step:@"4"];
        }else{
            [self password:@"4"];
        }
        NSLog(@"時計回りにRotate degrees: %f", degrees);
    } else if (degrees < -90) {
        if (_volume==true) {
            [self step:@"5"];
        }else{
            [self password:@"5"];
        };
        NSLog(@"反時計回りにRotate degrees: %f", degrees);
    }
}
//ピンチ動作検知時に呼ばれるメソッド
- (IBAction)pinchDetected:(UIGestureRecognizer *)sender {
    //ピンチ開始の2本の指の距離を1とした時
    //現在の2本の指の相対距離
    CGFloat scale = [(UIPinchGestureRecognizer *)sender scale];
    
    if (scale > 2.4) {
        NSLog(@"外向きにPinch scale: %f", scale);
        if (_volume==true) {
            [self step:@"6"];
        }else{
            [self password:@"6"];
        }
    } else if (scale < 0.4) {
        NSLog(@"内向きにPinch scale: %f", scale);
        if (_volume==true) {
            [self step:@"7"];
        }else{
            [self password:@"7"];
        }
    }
}


@end
